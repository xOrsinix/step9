package com.example.photoviewer

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlin.random.*

private lateinit var ivB:ImageView
private lateinit var btID:Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ivB=findViewById(R.id.ivB)
        ivB.setImageResource(R.drawable.kolpak)
        btID=findViewById(R.id.btID)
        btID.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                val img=when(Random.nextInt(1,6)){
                    1 -> R.drawable.kotek1
                    2 -> R.drawable.kotek2
                    3 -> R.drawable.kotek3
                    4 -> R.drawable.kotek4
                    5 -> R.drawable.kotek5
                    else -> R.drawable.kotek6
                }
                btID.setBackgroundColor(Color.argb(Random.nextInt(0,255),Random.nextInt(0,255),
                    Random.nextInt(0,255),Random.nextInt(0,255)))
                ivB.setImageResource(img)
            }
        })
    }
}